/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab1_1;

import java.util.Arrays;
import java.util.Collections;


/**
 *
 * @author HP
 */
public class ArrayManipulation {

    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        int sum = 0;
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[5];
        double maxValue = values[0];

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }

        values[0] = 1.1;
        values[1] = 2.2;
        values[2] = 3.3;
        values[3] = 4.4;
        values[4] = 5.5;

        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);
        }

        // Calculate the sum
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        // Print the sum
        System.out.println("6. Sum of all elements in the array: " + sum);

        for (int i = 1; i < values.length; i++) {
            if (values[i] > maxValue) {
                maxValue = values[i];
            }
        }

        // Print the maximum value
        System.out.println("Maximum value in the array: " + maxValue);
        
        // Create a new array to store the reversed elements
        String[] reversedNames = new String[names.length];

        // Fill the reversedNames array in reverse order
        for (int i = names.length - 1, j = 0; i >= 0; i--, j++) {
            reversedNames[j] = names[i];
        }

        // Print the reversedNames array
        System.out.println("Reversed names array:");
        for (String name : reversedNames) {
            System.out.println(name);
        }
        
        Arrays.sort( numbers );
        System.out.println(Arrays.toString( numbers )); 
    }
}
